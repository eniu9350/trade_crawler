'''
尝试登录支付宝
并获取账单记录

通过 cookies 登录支付宝，
'''

import requests
from selenium import webdriver
from bs4 import BeautifulSoup
from http.cookies import SimpleCookie
import threading
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver import Remote
from selenium.common.exceptions import WebDriverException
import webbrowser
import requests
import re
import bs4
import traceback
import time
import os.path
import random
import schedule
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import lxml.etree as etree
from lxml import html
import config


def get_data_fn(u):
    return './data/'+u['username'].replace('@', '_').replace('.', '_')+'.csv'


def get_cache_fn(u):
    return './temp/'+u['username'].replace('@', '_').replace('.', '_')+'.html'


def check_folder():
    if not os.path.exists('./data'):
        os.mkdir('./data')
    if not os.path.exists('./temp'):
        os.mkdir('./temp')


def parse(h, u):
    """
    parse response html and update data files
    """
    fn = get_data_fn(u)

    orig_rec_list = []
    if os.path.exists(fn):
        with open(fn) as f:
            orig_rec_list = f.readlines()
            if len(orig_rec_list) == 1 and len(orig_rec_list[0].strip()) == 0:
                orig_rec_list = []

    if len(orig_rec_list) == 0:
        rec_latest = ''
    else:
        rec_latest = orig_rec_list[len(orig_rec_list)-1].split(',')[2].strip()

    resp_tree = etree.HTML(h)
    rows = resp_tree.xpath('//table[@id="tradeRecordsIndex"]/tbody/tr')

    rec_list = []
    for row in rows:
        amount = float(row.xpath('td')[3].xpath(
            'span/text()')[0].replace(' ', ''))

        if amount < 0:
            continue

        order_id = row.xpath('td')[2].xpath('div/a')[0].attrib['title']
        if order_id == rec_latest:
            print('latest dup, break!')
            break

        tm = row.xpath('td')[1].xpath('p/text()')
        tm0 = tm[0].replace('\r', '').replace('\n', '').replace('\t', '')
        tm1 = tm[1].replace('\r', '').replace('\n', '').replace('\t', '')

        rec = tm0 + ' ' + tm1 + ',' + str(amount) + ',' + order_id
        rec_list.insert(0, rec)

    with open(fn, 'a+') as f:
        for rec in rec_list:
            f.write(rec+'\n')


def crawl(browser, u):
    """
    crawl trade details(using cookie)
    """
    browser.get('https://consumeprod.alipay.com/record/standard.htm')

    # save latest html source to cache
    with open(get_cache_fn(u), 'w+', encoding='utf-8') as f:
        f.write(browser.page_source)

    # clean
    soup = BeautifulSoup(browser.page_source, 'lxml')
    for script in soup(["script", "style"]):
        script.decompose()    # rip it out
    h = str(soup)

    # check if relogin is needed
    resp_tree = etree.HTML(h)
    if len(resp_tree.xpath('//div[@class="authcenter-body-login"]')) != 0:
        print('login needed!')
    else:
        # parse
        parse(h, u)

        # keep login state
        keep_login(browser)


def keep_login(browser):
    """
    keep login state
    """
    iv = config.options['emu_interval']

    # wait
    time.sleep(random.randint(1, iv))

    # click
    navItems = []
    navItems = navItems + browser.find_elements_by_xpath(
        '//li[contains(@class, "global-subnav-item")]/a')  # top nav
    navItems = navItems + \
        browser.find_elements_by_xpath(
            '//div[@id="J-consume-category"]/a')  # sub nav
    i = random.randint(1, len(navItems)) - 1
    navItems[i].click()


def init(u):
    """
    init browser
    """
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
        'Accept-Language': 'zh-CN,zh;q=0.9,en-XA;q=0.8,en;q=0.7',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36',
        # 'Host': 'auth.alipay.com' #ok
        'cookie': u['cookie'],
        'referer': 'https://auth.alipay.com/login/index.htm?goto=https%3A%2F%2Fconsumeprod.alipay.com%2Frecord%2Fadvanced.htm'  # del
    }
    cap = DesiredCapabilities.PHANTOMJS.copy()
    for key, value in headers.items():
        cap['phantomjs.page.customHeaders.{}'.format(key)] = value
    browser = webdriver.PhantomJS(
        executable_path=config.options['phantomjs_path'], desired_capabilities=cap)
    browser.delete_all_cookies()

    return browser


def job():
    for u in config.users:
        browser = init(u)
        crawl(browser, u)


if __name__ == '__main__':
    check_folder()
    schedule.every(config.options['crawl_interval']).seconds.do(job)

    while True:
        schedule.run_pending()
        time.sleep(1)
